cart = {
  "Items": 3,
  "Item": {
    "Apple iPhone 5S": {
      "productId": 688,
      "url": "http://website.com/phone_iphone5s.html",
      "price": 299.99
	  },
    "Solio Mono Solar Charger": {
      "productId": 655,
		  "url": "http://website.com/solio_charger.html",
		  "price": 29.95
	  },
    "24 Month Warranty Package": {
      "productId": 681,
		  "url": "http://website.com/24_month_warranty.html",
		  "price": 129.95
	  }
	},
  "Total": 459.89
};

function cartDetails(cart) {
  console.log("Items: "+ cart.Items);
  var items = cart.Item;
  for (var currentItem in items) {
    console.log("- " + currentItem + " ($" + items[currentItem].price + ")");
  }
  console.log("Total: " + cart.Total);
}

cartDetails(cart);
