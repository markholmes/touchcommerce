// Define initial variables
var state = document.getElementById("state");
var submit = document.getElementById("submit");
var statePurchasedIn = document.getElementById("state-purchased-in");
var statePurchasedInLabel = document.getElementById("state-purchased-in-label");
var fullName = document.getElementById("full-name");
var phoneNumber = document.getElementById("phone-number");
var description = document.getElementById("describe-question");

// The trade off to using ids is conflicting class specificity
statePurchasedIn.style.display = "none";
statePurchasedInLabel.style.display = "none";

// Initialize our event listeners
function initializeEvents() {
  state.addEventListener("change", handleStateSelection, false);
  submit.addEventListener("click", handleInputValidation, false);
}

function handleStateSelection() {
  var validDescription = document.getElementById("valid-description");
  var errorDescription = document.getElementById("error-description");
  var validState = document.getElementById("valid-state");
  var errorState = document.getElementById("error-state");
  var validPurchasedIn = document.getElementById("valid-purchased-in");
  var errorPurchasedIn = document.getElementById("error-purchased-in");
  if (state.value.toLowerCase() == "california") {
    statePurchasedIn.style.display = "block";
    statePurchasedInLabel.style.display = "block";
    if (validDescription.style.display === "block") {
      errorDescription.style.top = "440px";
      validDescription.style.top = "495px";
    }
    if (validState.style.display === "block") {
      validState.style.display = "none";
      errorState.style.display = "none";
      state.classList.remove('warn');
    }
  } else {
    statePurchasedIn.style.display = "none";
    statePurchasedInLabel.style.display = "none";
    if (validPurchasedIn.style.display === "block") {
      validPurchasedIn.style.display = "none";
      errorPurchasedIn.style.display = "none";
    }
    statePurchasedIn.classList.remove('warn');
    if (validDescription.style.display === "block") {
      errorDescription.style.top = "365px";
      validDescription.style.top = "425px";
    }
  }
}

function checkFullName() {
  var validFullName = document.getElementById("valid-full-name");
  var errorFullName = document.getElementById("error-full-name");
  if (fullName.value !== "") {
    validFullName.style.display = "none";
    errorFullName.style.display = "none";
    fullName.classList.remove('warn');
  } else {
    validFullName.style.display = "block";
    errorFullName.style.display = "block";
    fullName.classList.add('warn');
  }
}

function checkPhoneNumber() {
  var phoneRegex = /\d{3}[\-]\d{3}[\-]\d{4}/;
  var validPhoneNumber = document.getElementById("valid-phone-number");
  var errorPhoneNumber = document.getElementById("error-phone-number");
  if (phoneNumber.value !== "" && phoneRegex.test(phoneNumber.value) !== true) {
    validPhoneNumber.innerText = "Please enter a valid phone number.";
    validPhoneNumber.style.display = "block";
    errorPhoneNumber.style.display = "block";
    phoneNumber.classList.add('warn');
  } else if (phoneNumber.value !== "" && phoneRegex.test(phoneNumber.value) === true) {
    validPhoneNumber.style.display = "none";
    errorPhoneNumber.style.display = "none";
    phoneNumber.classList.remove('warn');
  } else {
    validPhoneNumber.innerText = "Mobile number is a required field.";
    validPhoneNumber.style.display = "block";
    errorPhoneNumber.style.display = "block";
    phoneNumber.classList.add('warn');
  }
}

function checkState() {
  var validState = document.getElementById("valid-state");
  var errorState = document.getElementById("error-state");
  if (state.value !== "select-one") {
    validState.style.display = "none";
    errorState.style.display = "none";
    state.classList.remove('warn');
  } else {
    validState.style.display = "block";
    errorState.style.display = "block";
    state.classList.add('warn');
  }
}

function checkStatePurchasedIn() {
  var validPurchasedIn = document.getElementById("valid-purchased-in");
  var errorPurchasedIn = document.getElementById("error-purchased-in");
  if (state.value.toLowerCase() == "california" && statePurchasedIn.value !== "select-one") {
    validPurchasedIn.style.display = "none";
    errorPurchasedIn.style.display = "none";
    statePurchasedIn.classList.remove('warn');
  } else if (state.value.toLowerCase() == "california" && statePurchasedIn.value === "select-one") {
    validPurchasedIn.style.display = "block";
    errorPurchasedIn.style.display = "block";
    statePurchasedIn.classList.add('warn');
  }
}

function checkDescription() {
  var validDescription = document.getElementById("valid-description");
  var errorDescription = document.getElementById("error-description");
  if (description.value !== "") {
    validDescription.style.display = "none";
    errorDescription.style.display = "none";
    description.classList.remove('warn');
  } else {
    validDescription.style.display = "block";
    errorDescription.style.display = "block";
    description.classList.add('warn');
  }
  if (statePurchasedIn.style.display === "block") {
    errorDescription.style.top = "440px";
    validDescription.style.top = "495px";
  } else {
    errorDescription.style.top = "365px";
    validDescription.style.top = "425px";
  }
}

function handleValidationSuccess() {
  if (fullName.value !== "" && phoneNumber.value !== "" && state.value !== "select-one" && ((state.value.toLowerCase() === "california" && statePurchasedIn.value !== "select-one") || (state.value.toLowerCase() !== "california" && state.value.toLowerCase() !== "select-one")) && description.value !== "") {
    document.getElementById("content").innerHTML = "<h1>Congratulations!! You have reached the chat agent.</h1>";
  }
}

// Handle input validation
function handleInputValidation() {
  checkFullName();
  checkPhoneNumber();
  checkState();
  checkStatePurchasedIn();
  checkDescription();
  handleValidationSuccess();
}

// Initialize events on load
window.addEventListener("load", initializeEvents, false);
